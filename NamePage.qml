import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Page {
    id: namePage

    header: Rectangle { //just as a margin
        height: 10
    }

    INIReader {
        id: iniReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Name"
    }

    GitManager {
        id: gitManager
    }

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: nameGrid
            columns: 2
            anchors.centerIn: parent
            rowSpacing: firstName.height / 2

            Label {text: "First Name"}
            TextField {
                id:firstName;
                placeholderText: "Enter First Name"
                text: iniReader.getVarible("FirstName");
            }

            Label {text: "Middle Name"}
            TextField{id:middleName;
                placeholderText: "Enter middle Name"
                text: iniReader.getVarible("MiddleName");
            }

            Label {text: "Last Name"}
            TextField{id:lastName;
                placeholderText: "Enter last Name"
                text: iniReader.getVarible("LastName");
            }

            Label {text: "Preferred Name"}
            TextField{id:preferredName;
                placeholderText: "What name does the patient prefer"
                text: iniReader.getVarible("PreferredName")
            }

            Label {text: "How to phonetically say the same"}
            TextField{id:namePhonetic;
                placeholderText: "How to phonetically say the name"
                text: iniReader.getVarible("NamePhonetic")
            }
        }
    }

    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            nameItems: {"FirstName":firstName.text,
                        "MiddleName":middleName.text,
                        "LastName":lastName.text,
                        "PreferredName": preferredName.text,
                        "NamePhonetic": namePhonetic.text}

        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }

}
