import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0


Page {
    id: phonePage

    header: Rectangle {
        height: 10
    }

    INIReader {
        id: iniReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Phones"
    }

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: phoneGrid
            columns: 2
            anchors.centerIn: parent
            rowSpacing: homePhone.height / 2

            Label {text: "Home Phone Number"}
            TextField{ //TODO: Format the text as a phone number
                id:homePhone;
                placeholderText: "Add \"+\" if international"
                inputMethodHints: Qt.ImhDialableCharactersOnly
                text: iniReader.getVarible("HomePhone")
            }

            Label {text: "Cell Phone Number"}
            TextField{
                id:cellPhone;
                placeholderText: "Add \"+\" if international"
                inputMethodHints: Qt.ImhDialableCharactersOnly
                text: iniReader.getVarible("CellPhone")
            }
        }
    }


    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            nameItems: {"HomePhone":homePhone.text,
                        "CellPhone":cellPhone.text
            }

        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }
}
