#include "cdgitmanager.h"
#include "cddefaults.h"

#include <QProcess>
#include <QDir>
#include <QDebug>

#define GITBIN QString("/usr/bin/git")

CDGitManager::CDGitManager(QObject *parent) : QObject(parent)
{

}

QString CDGitManager::commitData(QString patientName, QString comment) {
    QString commitPath = CDDefaults::getDefaultPatDir() + patientName;
    //check if existing .git repo
    QDir gitDir = commitPath + "/.git/";
    QProcess pro;
    pro.setProgram(GITBIN);
    pro.setWorkingDirectory(commitPath);
    if(!gitDir.exists()) {
        //init the repo
        QStringList initArgs;
        initArgs.append("init");
        pro.setArguments(initArgs);
        pro.start();
        pro.waitForFinished();
    }

    //Add any possible files
    QStringList addArgs;
    addArgs.append("add");
    addArgs.append("--all");
    pro.setArguments(addArgs);
    pro.start();
    pro.waitForFinished();

    //commit with comment
    QStringList commitArgs;
    commitArgs.append("commit");
    commitArgs.append("-a");
    commitArgs.append("-m");
    commitArgs.append("\"" + comment + "\"");
    pro.setArguments(commitArgs);
    pro.start();
    pro.waitForFinished();
    QString returnMe = "Done: " + pro.readAllStandardOutput();
    if(pro.exitCode() != 0) {
        //returnMe = "Error: " + pro();
    }
    qDebug()<<returnMe;
    return returnMe;
}
