import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Page {
    id: emailPage

    header: Rectangle {
        height: 10
    }

    INIReader {
        id: iniReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Emails"
    }

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: emailGrid
            columns: 2
            anchors.centerIn: parent
            rowSpacing: homeEmail.height / 2

            Label {text: "Regular Email"}
            TextField{
                id:homeEmail;
                placeholderText: "Patient's regular e-mail"
                text: iniReader.getVarible("Email");
                inputMethodHints: Qt.ImhEmailCharactersOnly
            }

            Label {text: "Facebook ID"}
            TextField{
                id:facebookID;
                placeholderText: "Enter in Facebook ID/URL"
                text: iniReader.getVarible("Facebook")
            }

            Label {text: "Instagram ID"}
            TextField{
                id:instagramID;
                placeholderText: "Enter in Instagram ID/URL"
                text: iniReader.getVarible("Instagram")
            }

            Label {text: "WhatsApp ID"}
            TextField{
                id:whatsAppID;
                placeholderText: "Enter in WhatsApp ID/URL"
                text: iniReader.getVarible("WhatsApp")
            }
        }
    }


    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            nameItems: {"Email":homeEmail.text,
                        "Facebook":facebookID.text,
                        "Instagram":instagramID.text,
                        "WhatsApp":whatsAppID.text
            }
        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }

}
