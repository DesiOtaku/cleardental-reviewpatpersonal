import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Page {
    id: addrPage

    header: Rectangle {
        height: 10
    }

    INIReader {
        id: iniReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Address"
    }

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: addrGrid
            columns: 2
            anchors.centerIn: parent
            rowSpacing: addr1.height / 4

            Label {text: "Street Address (Line 1)"}
            TextField {
                id:addr1;
                placeholderText: "Street Address (Line 1)"
                text: iniReader.getVarible("StreetAddr1");
            }

            Label {text: "Street Address (Line 2)"}
            TextField {
                id:addr2;
                placeholderText: "Street Address (Line 2) if needed"
                text: iniReader.getVarible("StreetAddr2");
            }

            Label {text: "City"}
            TextField{
                id:city;
                placeholderText: "City of the patient"
                text: iniReader.getVarible("City");
            }

            Label {text: "State"}
            ComboBox {
                id: state
                model: ["AK","AL","AR","AZ","CA","CO",  "CT","DC","DE","FL","GA","GU",
                    "HI","IA","ID", "IL","IN","KS","KY","LA","MA","MD","ME","MH","MI",
                    "MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY", "OH",
                    "OK","OR","PA","PR","PW","RI","SC","SD","TN","TX","UT","VA","VI",
                    "VT","WA","WI","WV","WY"]
                Component.onCompleted: state.currentIndex =
                                       state.find(iniReader.getVarible("State"));
            }

            Label {text: "Zip code"}
            TextField{
                id: zip
                placeholderText: "Zip/Postal code"
                inputMethodHints: Qt.ImhDigitsOnly
                text: iniReader.getVarible("Zip");

            }

            Label {text: "Country"}
            TextField{id:country
                placeholderText: "If not in USA, enter country"
                text: iniReader.getVarible("Country");
            }
        }
    }


    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            nameItems: { "StreetAddr1":addr1.text,
                         "StreetAddr2":addr2.text,
                         "City":city.text,
                         "State":state.currentText,
                         "Zip":zip.text,
                         "Country":country.text
            }
        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }

}
