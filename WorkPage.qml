import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Page {
    id: workPage

    header: Rectangle {
        height: 10
    }

    INIReader {
        id: iniReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Work"
    }

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: workGrid
            columns: 2
            anchors.centerIn: parent
            rowSpacing: workAddr1.height / 4

            Label {text: "Work Street Address (Line 1)"}
            TextField{
                id:workAddr1;
                placeholderText: "Work Street Address (Line 1)"
                text: iniReader.getVarible("WorkAddr1");
            }

            Label {text: "Work Street Address (Line 2)"}
            TextField{
                id:workAddr2;
                placeholderText: "Work Street Address (Line 2)"
                text: iniReader.getVarible("WorkAddr2");
            }

            Label {text: "Work City"}
            TextField{
                id:workCity;
                placeholderText: "Enter work City"
                text: iniReader.getVarible("WorkCity");
            }

            Label {text: "Work State"}
            ComboBox {
                id: workState
                model: ["AK","AL","AR","AZ","CA","CO","CT","DC","DE","FL","GA","GU",
                    "HI","IA","ID", "IL","IN","KS","KY","LA","MA","MD","ME","MH","MI",
                    "MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY", "OH",
                    "OK","OR","PA","PR","PW","RI","SC","SD","TN","TX","UT","VA","VI",
                    "VT","WA","WI","WV","WY"]
                Component.onCompleted: workState.currentIndex =
                                       workState.find(iniReader.getVarible("WorkState"));
            }

            Label {text: "Work Zip code"}
            TextField{
                id:workZip;
                placeholderText: "Work Zip code"
                text: iniReader.getVarible("WorkZip");
            }

            Label {text: "Country (if not USA)"}
            TextField{
                id:workCountry;
                placeholderText: "If not in USA, enter country"
                text: iniReader.getVarible("WorkCountry");
            }
        }
    }


    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            nameItems: { "WorkAddr1":workAddr1.text,
                         "WorkAddr2":workAddr2.text,
                         "WorkCity":workCity.text,
                         "WorkState":workState.currentText,
                         "WorkZip":workZip.text,
                         "WorkCountry":workCountry.text
            }
        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }

}
