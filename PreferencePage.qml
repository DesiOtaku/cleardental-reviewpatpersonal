import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0


Page {
    id: preferencePage

    header: Rectangle {
        height: 10
    }

    INIReader {
        id: iniReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Preferences"
    }

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: preferenceGrid
            columns: 2
            anchors.centerIn: parent
            rowSpacing: lang.height / 2

            Label {text: "Preferred Language"}
            ComboBox {
                id: lang
                editable: false
                textRole: "key"
                currentIndex: 0
                model: ListModel {
                    id: langModel
                     ListElement { key: "English"; value: "en" }
                     ListElement { key: "Spanish"; value: "es" }
                     ListElement { key: "French"; value: "fr" }
                     ListElement { key: "Chinese"; value: "zh" }
                     //TODO: add many more languages
                }
                Component.onCompleted: {
                    var langValue = iniReader.getVarible("PreferredLanguage");
                    for(var i=0;i<langModel.count;i++) {
                        if(langModel.get(i).value === langValue) {
                            lang.currentIndex = i;
                        }
                    }
                }
            }

            Label {text: "Available Days"}

            Row {
                id: dayRow

                function getDays() {
                    var returnMe="";
                    returnMe += (sundayBox.checked ? "Su " : "");
                    returnMe += (mondayBox.checked ? "M " : "");
                    returnMe += (tuesdayBox.checked ? "Tu " : "");
                    returnMe += (wednesdayBox.checked ? "W " : "");
                    returnMe += (thursdayBox.checked ? "Th " : "");
                    returnMe += (fridayBox.checked ? "F " : "");
                    returnMe += (saturdayBox.checked ? "Sa " : "");
                    return returnMe;
                }

                CheckBox {
                    id: sundayBox
                    text: "Sunday"
                } CheckBox {
                    id: mondayBox
                    text: "Monday"
                } CheckBox {
                    id: tuesdayBox
                    text: "Tuesday"
                } CheckBox {
                    id: wednesdayBox
                    text: "Wednesday"
                } CheckBox {
                    id: thursdayBox
                    text: "Thursday"
                } CheckBox {
                    id: fridayBox
                    text: "Friday"
                } CheckBox {
                    id: saturdayBox
                    text: "Saturday"
                }

                Component.onCompleted: {
                    var theDays = iniReader.getVarible("AvailableDays");
                    sundayBox.checked = (theDays.search("Su") > -1 );
                    mondayBox.checked = (theDays.search("M") > -1 );
                    tuesdayBox.checked = (theDays.search("Tu") > -1 );
                    wednesdayBox.checked = (theDays.search("W") > -1 );
                    thursdayBox.checked = (theDays.search("Th") > -1 );
                    fridayBox.checked = (theDays.search("F") > -1 );
                    saturdayBox.checked = (theDays.search("Sa") > -1 );
                }
            }

            Label {text: "Preferred Time"}
            Row {
                spacing: lang.height/2
                Label {
                    text: "From"
                    anchors.verticalCenter: parent.verticalCenter
                }
                SpinBox {
                    id:fromTime
                    from: 0
                    to: 23
                    value: {
                        var theTime = iniReader.getVarible("AvailableTime");
                        if(theTime.length > 0) {
                            var splitted = theTime.split("-");
                            return splitted[0];
                        }
                        return 8;
                    }
                    textFromValue: function(value, locale) {
                        if(value < 12) {
                            return (value+1) + " " + "AM"
                        } else {
                            return (value-11) + " " + "PM"
                        }
                    }
                }
                Label {
                    text: "to"
                    anchors.verticalCenter: parent.verticalCenter
                }
                SpinBox {
                    id:toTime
                    from: 1
                    to: 24
                    value: {
                        var theTime = iniReader.getVarible("AvailableTime");
                        if(theTime.length > 0) {
                            var splitted = theTime.split("-");
                            return splitted[1];
                        }
                        return 15;
                    }
                    textFromValue: function(value, locale) {
                        if(value < 12) {
                            return (value+1) + " " + "AM"
                        } else {
                            return (value-11) + " " + "PM"
                        }
                    }
                }
            }

            Label {text: "Preferred Contact Method"}
            ComboBox {
                id: prefContact
                model: ["Call Phone","Text", "E-mail", "Work Phone", "Facebook", "Instagram",
                    "WhatsApp"]
                Component.onCompleted: prefContact.currentIndex =
                                       prefContact.find(iniReader.getVarible("PreferredContact"));
            }

            Label {text: "Preferred Doctor"}
            ComboBox {
                id: prefDoctor
                editable: true
                editText: iniReader.getVarible("PreferredDoctor")
            }
        }
    }


    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            nameItems: {"PreferredLanguage":langModel.get(lang.currentIndex).value,
                        "AvailableDays":dayRow.getDays(),
                        "AvailableTime":fromTime.value + "-" + toTime.value,
                        "PreferredContact":prefContact.currentText,
                        "PreferredDoctor":prefDoctor.editText
            }
        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }

}
