#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "cdinireader.h"
#include "cdgitmanager.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QGuiApplication app(argc, argv);

    qmlRegisterType<CDINIReader>("dental.clear", 1, 0, "INIReader");
    qmlRegisterType<CDGitManager>("dental.clear", 1, 0, "GitManager");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("patientFileName",argv[1]);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
