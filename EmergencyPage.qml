import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Page {
    id: emegencyPage

    header: Rectangle {
        height: 10
    }

    INIReader {
        id: iniReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Emergency"
    }

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: emergencyGrid
            columns: 2
            anchors.centerIn: parent
            rowSpacing: emergencyName.height / 4

            Label {text: "Emergency Contact's Name"}
            TextField {
                id:emergencyName;
                placeholderText: "Emergency Contact's Name"
                text: iniReader.getVarible("EmergencyName")
            }

            Label {text: "Emergency Contact's Phone"}
            TextField {
                id:emergencyPhone;
                placeholderText: "Emergency Contact's Phone"
                text: iniReader.getVarible("EmergencyPhone")
                inputMethodHints: Qt.ImhDialableCharactersOnly
            }

            Label {text: "Relationship with patient"}
            TextField {
                id:emergencyRelation;
                placeholderText: "Relationship with patient"
                text: iniReader.getVarible("EmergencyRelation")
            }
        }
    }


    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            nameItems: {"EmergencyName":emergencyName.text,
                        "EmergencyPhone":emergencyPhone.text,
                        "EmergencyRelation":emergencyRelation.text
            }

        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }

}
