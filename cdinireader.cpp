#include "cdinireader.h"
#include "cddefaults.h"
#include "cdgitmanager.h"

#include <QDebug>
#include <QCoreApplication>

CDINIReader::CDINIReader(QObject *parent) : QObject(parent) {
    m_patName="";
    m_groupName="";
    m_infoType = Invalid;
    m_Settings = nullptr;
}

CDINIReader::~CDINIReader() {}

void CDINIReader::setPatientName(QString newPatName) {
    m_patName = newPatName;
}

QString CDINIReader::getPatientName() {
    return m_patName;
}

void CDINIReader::setInfoType(InfoType newInfoType) {
    m_infoType = newInfoType;
}

CDINIReader::InfoType CDINIReader::getInfoType() {
    return m_infoType;
}

void CDINIReader::setGroupName(QString newGroupName) {
    m_groupName = newGroupName;
}

QString CDINIReader::getGroupName() {
    return m_groupName;
}

QString CDINIReader::getVarible(QString variableName) {
    makeSettings();
    if(m_Settings) {
        return m_Settings->value(variableName).toString();
    }
    return QString("");
}

void CDINIReader::makeSettings() {
    if(!m_Settings) {
        m_patName = QCoreApplication::arguments().at(1);
        if(m_patName.length() && m_infoType && m_groupName.length()) {
            QString fName="";
            switch (m_infoType) {
            case InfoType::Personal:
                fName = "personal.ini";
                break;
            default:
                break;
            }
            m_Settings = new QSettings(CDDefaults::getDefaultPatDir() +
                                       m_patName + "/" + fName,QSettings::IniFormat);
            m_Settings->beginGroup(m_groupName);
        }
    }
}

void CDINIReader::setVariable(QString variableName, QString value) {
    makeSettings(); //will make it if needed
    if(m_Settings) {
        m_Settings->setValue(variableName,value);
    }
}

void CDINIReader::commitChanges() {
    if(m_Settings) {
        //Just in case... probably never needed
        m_Settings->endGroup();
        m_Settings->beginGroup("Others");
        m_Settings->setValue("SpecVersion",1);
        m_Settings->endGroup();
        m_Settings->beginGroup(m_groupName);

        m_Settings->sync();
        CDGitManager::commitData(m_patName, "This is another test!");
    }
}
