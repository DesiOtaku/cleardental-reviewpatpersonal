import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2

ApplicationWindow {
    id: rootWin
    visible: true
    width: 1024
    height: 768
    title: qsTr("Edit Personal Information: " + patientFileName)

    header: TabBar {
        id: bar
        width: parent.width
        TabButton {
            text: qsTr("Name")
        }
        TabButton {
            text: qsTr("Personal")
        }
        TabButton {
            text: qsTr("Home Address")
        }
        TabButton {
            text: qsTr("Phone(s)")
        }
        TabButton {
            text: qsTr("Email / Social Media")
        }
        TabButton {
            text: qsTr("Work Information")
        }
        TabButton {
            text: qsTr("Emergency Contact")
        }
        TabButton {
            text: qsTr("Preferences")
        }
    }

    SwipeView   {
        id: view
        anchors.top: bar.bottom
        anchors.left: rootWin.contentItem.left
        anchors.right: rootWin.contentItem.right
        anchors.bottom: rootWin.contentItem.bottom
        currentIndex: bar.currentIndex
        interactive: false

        NamePage{}
        PersonalPage{}
        AddressPage{}
        PhonePage{}
        EmailPage{}
        WorkPage{}
        EmergencyPage{}
        PreferencePage{}
    }

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: rootWin.height
        width: rootWin.width
        active: false

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: rootWin.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 300
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}
