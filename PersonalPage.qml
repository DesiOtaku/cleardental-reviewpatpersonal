import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Page {
    id: personalPage

    header: Rectangle { //just as a margin
        height: 10
    }

    INIReader {
        id: iniReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Personal"
    }

    GitManager {
        id: gitManager
    }

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: nameGrid
            columns: 2
            anchors.centerIn: parent
            rowSpacing: gender.height / 2

            Label{text:"Date of Birth"}
            TumbleDatePicker{ id:patDOB
                Component.onCompleted: setDate(iniReader.getVarible("DateOfBirth"));
            }

            Label{text:"Sex"}
            ButtonGroup { id: radioGroup }
            Row {
                id: sexRow
                function getSex() {
                    if(maleRadio.checked) {
                        return "Male";
                    } else {
                        return "Female";
                    }
                }

                RadioButton {
                    id: maleRadio
                    checked: true
                    text: qsTr("Male")
                    ButtonGroup.group: radioGroup
                }

                RadioButton {
                    id: femaleRadio
                    text: qsTr("Female")
                    ButtonGroup.group: radioGroup
                }

                Component.onCompleted: {
                    if(iniReader.getVarible("Sex") === "Female") {
                        femaleRadio.checked = true;
                    }
                }
            }

            Label{text:"Gender (if different)"}
            TextField{id:gender;
                placeholderText: "Put in the gender if different from sex"
                text: iniReader.getVarible("Gender");
            }

            Label{text:"Race"}
            ComboBox {
                id: race
                implicitWidth: patDOB.width
                model: ["I choose not to answer","American Indian or Alaska native",
                    "Asian", "Black or African American", "Polynesian","White","Other"]
                /*currentIndex:  {
                  not working: https://bugreports.qt.io/browse/QTBUG-73716
                    return race.find(iniReader.getVarible("Race"));
                }*/
                Component.onCompleted: race.currentIndex = race.find(iniReader.getVarible("Race"));
            }

            Label{text:"Ethnicity"}
            ComboBox {
                id: ethnicity
                implicitWidth: patDOB.width
                model: ["I choose not to answer","Hispanic Or Latino",
                    "Not Hispanic or Latino"]
                Component.onCompleted: ethnicity.currentIndex =
                                       ethnicity.find(iniReader.getVarible("Ethnicity"));
            }
        }
    }

    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            nameItems: {"DateOfBirth":patDOB.getDate(),
                        "Sex":sexRow.getSex(),
                        "Gender":gender.text,
                        "Race":race.currentText,
                        "Ethnicity":ethnicity.currentText
            }

        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }

}
