#ifndef CDINIREADER_H
#define CDINIREADER_H

#include <QObject>
#include <QSettings>

class CDINIReader : public QObject
{
    Q_OBJECT
public:
    enum InfoType {
        Invalid,
        Personal,
        Dental,
        Periodontal,
        HardTissue,
        SoftTissue
    };
    Q_ENUM(InfoType)

    CDINIReader(QObject *parent = nullptr);
    ~CDINIReader();

    Q_PROPERTY(QString patientName READ getPatientName WRITE setPatientName)
    Q_PROPERTY(InfoType infoType READ getInfoType WRITE setInfoType)
    Q_PROPERTY(QString groupName READ getGroupName WRITE setGroupName)

signals:

public slots:
    void setPatientName(QString newPatName);
    QString getPatientName();

    void setInfoType(InfoType newInfoType);
    InfoType getInfoType();

    void setGroupName(QString newGroupName);
    QString getGroupName();

    QString getVarible(QString variableName);
    void setVariable(QString variableName, QString value);

    void commitChanges();

private:
    QString m_patName;
    InfoType m_infoType;
    QString m_groupName;
    QSettings *m_Settings;

    void makeSettings();
};

#endif // CDINIREADER_H
